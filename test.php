<!DOCTYPE html>
<html>
<head>
<title>Stil de viata</title>
</head>
<body>

<h1>Sfaturi pentru un stil de viata sanatos si sportiv</h1>
<p>Sunt sigur ca multi dintre voi deja sunteti constienti ca nu doar sportul sau
alimentatia sunt suficiente pentru un stil de viata sanatos, aceste elemente cheie
merg impreuna cot la cot precum doi betivi care merg pe strada sprijinindu-se unul
de altul pentru a-si pastra echilibrul. Alimentatia conteaza in proportie de 70% si
exercitiul in proportie de 30% pentru acest stil de viata.</p>
<hr>
<h1>Ce trebuie sa eviti:</h1>
<p>Cum alimentatia conteaza atat de mult pentru o viata mai sanatoasa,
inseamna ca este si mai complicat. De ce? Pentru ca asta inseamna sa renunti la 
zahar, grasimi si sare. Par a fi doar trei chesti insa derivatele lor sunt cele care 
pot fi enumerate: dulciurile si prajelile.</p>
<h1>Ce iti recomand:</h1>
<ul>
    <li>Sa ai 4 mese/zi si aproape la acelasi interval orar. Mesele sa nu fie prea bogate ca si cantitate ci calitative.</li>
    <li>Sa ai un aport de 1g/proteine/kg. Cantaresti 80kg => 80g proteina/zi.</li>
    <li>Sa bei cel putin 2l/zi de apa. Poti bea apa chiar daca nu iti este sete.</li>
    <li>Sa te odihnesti, asta nu inseamna sa dormi neaparat mult, dormi interval 22:30 - 06:30 spre exemplu. Din punctul meu de vedere 6-7 ore de somn/noapte sunt suficiente.</li>
    <li>Sa consumi alimente bogate in fibre</li>
    <li>Incearca pe cat posibil sa echilibrezi mesele(cata carne ai in farfurie, cel putin atat de putine legume).</li>
    <li>Mananca fructe intre mese.</li>
</ul>
</body>
</html>